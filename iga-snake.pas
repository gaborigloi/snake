program iga_snake;           {készítette: Iglói Gábor 8.C Free Pascallal}
uses crt;   // ESC: #27 ENTER: #13 FEL: #72 LE: #80 JOBB: #77 BAL: #75
 // integer integer integer iinteger integer integer integer integer integer
Type snake=record
     x,y:integer;
    end;
     rek=record
     nev:string;
     pontsz:integer;
    end;

Var sebesseg,idohatar,kezdohossz:integer;
    bt,rk:text;
    reks: array[1..200] of rek;
    rsz:integer;
    exit1,exit2,exit3:boolean;

function AnsiToAscii(s:string):string;  // windowsos -> konzolos
var n:integer;
Begin
  AnsiToAscii:=s;                       // ez lesz a függvény visszatérési értéke
  for n:=1 to Length(s) do
    Begin
      case AnsiToAscii[n] of
        #225: AnsiToAscii[n] := #160;   // á
        #193: AnsiToAscii[n] := #181;   // Á
        #233: AnsiToAscii[n] := #130;   // é
        #201: AnsiToAscii[n] := #144;   // É
        #237: AnsiToAscii[n] := #161;   // í
        #205: AnsiToAscii[n] := #146;   // Í
        #243: AnsiToAscii[n] := #162;   // ó
        #211: AnsiToAscii[n] := #224;   // Ó
        #246: AnsiToAscii[n] := #148;   // ö
        #214: AnsiToAscii[n] := #153;   // Ö
        #245: AnsiToAscii[n] := #139;   // ő
        #213: AnsiToAscii[n] := #138;   // Ő
        #250: AnsiToAscii[n] := #163;   // ú
        #218: AnsiToAscii[n] := #233;   // Ú
        #252: AnsiToAscii[n] := #129;   // ü
        #220: AnsiToAscii[n] := #154;   // Ü
        #251: AnsiToAscii[n] := #251;   // ű
        #219: AnsiToAscii[n] := #235;   // Ú
    end;
  end;
End;

Procedure Rekh(const pont:integer);
Forward;


Procedure Ketsz();
Var s,s2:Array [1..200] of snake;
    p: array [1..10] of snake;
    h,h2,i:integer;
    c:char;
    hal:boolean;
    irany,irany2:string;
Begin
clrscr;
randomize;
h:=kezdohossz;
h2:=kezdohossz;
For i:=1 to 200 do s[i].y:=10+i;
For i:=1 to 200 do s[i].x:=20;
For i:=1 to 200 do s2[i].y:=10+i;
For i:=1 to 200 do s2[i].x:=10;
For i:=1 to 10 do p[i].x:=random(47)+2;
For i:=1 to 10 do p[i].y:=random(22)+3;
hal:=false;
irany:='';
irany2:='';
delay(100);
 {pi:=0;}
for i:= 1 to 50 do begin GotoXY(i,2); write('-'); gotoXY(i,25); write('-'); end;
for i:=3 to 25 do begin GotoXY(1,i); write('|'); GotoXY(50,i); write('|'); end;
textcolor(green);
for i:=1 to h do
begin
GotoXY(s[i].x,s[i].y);
write('O');
end;
textcolor(red);
for i:=1 to h2 do
begin
GotoXY(s2[i].x,s2[i].y);
write('O');
end;
repeat
{ pi:=pi+1;
if pi MOD 24 =0 then begin
i:=random(10);
p[i].x:=random(47)+2;
p[i].y:=random(22)+3;
end;}
randomize;
gotoXY(7,1);
textcolor(yellow);
textbackground(black);
write('SCORE: ',h+h2-kezdohossz*2);
if keypressed then c:= readkey;
case c of
#27: hal:=true;
#72: irany:='fel';
#80: irany:='le';
#77: irany:='jobb';
#75: irany:='bal';
#119: irany2:='fel';
#115: irany2:='le';
#100: irany2:='jobb';
#97: irany2:='bal';
end;
If irany <> '' then begin
GotoXY(s[h].x,s[h].y);
write(' ');
for i:=h downto 2 do begin s[i].x:=s[i-1].x; s[i].y:=s[i-1].y;   end;
end;
If irany2 <> '' then begin
GotoXY(s2[h2].x,s2[h2].y);
write(' ');
for i:=h2 downto 2 do begin s2[i].x:=s2[i-1].x; s2[i].y:=s2[i-1].y;   end;
end;
If irany='fel' then s[1].y:=s[1].y-1;
If irany='le' then s[1].y:=s[1].y+1;
If irany='jobb' then s[1].x:=s[1].x+1;
If irany='bal' then s[1].x:=s[1].x-1;
If irany2='fel' then s2[1].y:=s2[1].y-1;
If irany2='le' then s2[1].y:=s2[1].y+1;
If irany2='jobb' then s2[1].x:=s2[1].x+1;
If irany2='bal' then s2[1].x:=s2[1].x-1;
for i:=1 to 10 do
begin
if (s[1].y=p[i].y) and (s[1].x=p[i].x) then begin h:=h+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=1 to 10 do
begin
if (s2[1].y=p[i].y) and (s2[1].x=p[i].x) then begin h2:=h2+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=2 to h do
begin
if (s[1].y=s[i].y) and (s[1].x=s[i].x) then hal:=true;
end;
for i:=2 to h2 do
begin
if (s2[1].y=s2[i].y) and (s2[1].x=s2[i].x) then hal:=true;
end;
for i:=2 to h do
begin
if (s2[1].y=s[i].y) and (s2[1].x=s[i].x) then hal:=true;
end;
for i:=2 to h2 do
begin
if (s[1].y=s2[i].y) and (s[1].x=s2[i].x) then hal:=true;
end;
If s[1].x> 49 then hal:=true;
If s[1].y> 24 then hal:=true;
If s[1].y < 3 then hal:=true;
If s[1].x < 2 then hal:=true;
If s2[1].x> 49 then hal:=true;
If s2[1].y> 24 then hal:=true;
If s2[1].y < 3 then hal:=true;
If s2[1].x < 2 then hal:=true;
textcolor(yellow);
for i:=1 to 10 do
begin
GotoXY(p[i].x,p[i].y);
write('*');
end;
textcolor(green);
GotoXY(s[1].x,s[1].y);
Write('O');
textcolor(red);
GotoXY(s2[1].x,s2[1].y);
Write('O');
delay(sebesseg+30);

until hal;
textcolor(white);
textbackground(red);
GotoXY(23,12);
write('Game over');
textcolor(white);
textbackground(white);
delay(1500);
Rekh(h+h2-kezdohossz*2);
End;


Procedure P3();
Var s:Array [1..200] of snake;
    p: array [1..10] of snake;
    h,i,t:integer;
    c:char;
    hal:boolean;
    irany:string;
Begin
clrscr;
randomize;
h:=kezdohossz;
For i:=1 to 200 do s[i].y:=15+i;
For i:=1 to 200 do s[i].x:=20;
For i:=1 to 10 do p[i].x:=random(47)+2;
For i:=1 to 10 do p[i].y:=random(22)+3;
hal:=false;
irany:='';
t:=idohatar;
delay(100);
{pi:=0;}
textcolor(white);
for i:= 1 to 50 do begin gotoXY(i,12); write('-'); end;
for i:=3 to 25 do begin GotoXY(25,i); write('|'); end;
textcolor(green);
for i:=1 to h do
begin
GotoXY(s[i].x,s[i].y);
write('O');
end;
repeat
randomize;
{pi:=pi+1;
if pi MOD 24 =0 then begin
i:=random(10);
p[i].x:=random(47)+2;
p[i].y:=random(22)+3;
end;}
gotoXY(7,1);
textcolor(yellow);
textbackground(black);
write('SCORE: ',h-kezdohossz);
gotoXY(23,1); write('        ');
gotoXY(20,1);
write('x: ',s[1].x,' y: ',s[1].y);
t:=t-1;
gotoXY(41,1);
write('TIME: ',t);
if keypressed then c:= readkey;
case c of
#27: hal:=true;
#72: irany:='fel';
#80: irany:='le';
#77: irany:='jobb';
#75: irany:='bal';
else irany:='';
end;
If irany <> '' then begin
GotoXY(s[h].x,s[h].y);
write(' ');
for i:=h downto 2 do begin s[i].x:=s[i-1].x; s[i].y:=s[i-1].y;   end;
end;
If irany='fel' then s[1].y:=s[1].y-1;
If irany='le' then s[1].y:=s[1].y+1;
If irany='jobb' then s[1].x:=s[1].x+1;
If irany='bal' then s[1].x:=s[1].x-1;
If s[1].x=-1 then s[1].x:=50;
If s[1].x=51 then s[1].x:=1;
If s[1].y=1 then s[1].y:=25;
If s[1].y=26 then s[1].y:=2;
for i:=1 to 10 do
begin
if (s[1].y=p[i].y) and (s[1].x=p[i].x) then begin h:=h+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=2 to h do
begin
if (s[1].y=s[i].y) and (s[1].x=s[i].x) then hal:=true;
end;
If s[1].x= 25 then hal:=true;
If s[1].y= 12 then hal:=true;
If t=0 then hal:=true;
textcolor(yellow);
for i:=1 to 10 do
begin
GotoXY(p[i].x,p[i].y);
write('*');
end;
textcolor(green);
GotoXY(s[1].x,s[1].y);
write('O');
If (irany='jobb') or (irany='bal') then delay(sebesseg)
else delay(sebesseg*2-20);

until hal;
textcolor(white);
textbackground(red);
GotoXY(23,12);
write('Game over');
textcolor(white);
textbackground(white);
delay(1500);
Rekh(h-kezdohossz);
End;


Procedure P2();
Var s:Array [1..200] of snake;
    p: array [1..10] of snake;
    h,i,t:integer;
    c:char;
    hal:boolean;
    irany:string;
Begin
clrscr;
randomize;
h:=kezdohossz;
For i:=1 to 200 do s[i].y:=10+i;
For i:=1 to 200 do s[i].x:=20;
For i:=1 to 10 do p[i].x:=random(47)+2;
For i:=1 to 10 do p[i].y:=random(22)+3;
hal:=false;
irany:='';
t:=idohatar;
delay(100);
{pi:=0;}
textcolor(white);
for i:= 1 to 50 do begin GotoXY(i,2); write('-'); gotoXY(i,25); write('-'); end;
for i:=3 to 25 do begin GotoXY(1,i); write('|'); GotoXY(50,i); write('|'); end;
GotoXY(30,10);
write('----------');
for i:=11 to 19 do Begin GotoXY(30,i); write('|        |'); end;
gotoXY(30,20);
write('----------');
textcolor(green);
for i:=1 to h do
begin
GotoXY(s[i].x,s[i].y);
write('O');
end;
repeat
randomize;
{pi:=pi+1;
if pi MOD 24 =0 then begin
i:=random(10);
p[i].x:=random(47)+2;
p[i].y:=random(22)+3;
end;}
gotoXY(7,1);
textcolor(yellow);
textbackground(black);
write('SCORE: ',h-kezdohossz);
gotoXY(23,1); write('        ');
gotoXY(20,1);
write('x: ',s[1].x,' y: ',s[1].y);
t:=t-1;
gotoXY(41,1);
write('TIME: ',t);
if keypressed then c:= readkey;
case c of
#27: hal:=true;
#72: irany:='fel';
#80: irany:='le';
#77: irany:='jobb';
#75: irany:='bal';
else irany:='';
end;
If irany <> '' then begin
GotoXY(s[h].x,s[h].y);
write(' ');
for i:=h downto 2 do begin s[i].x:=s[i-1].x; s[i].y:=s[i-1].y;   end;
end;
If irany='fel' then s[1].y:=s[1].y-1;
If irany='le' then s[1].y:=s[1].y+1;
If irany='jobb' then s[1].x:=s[1].x+1;
If irany='bal' then s[1].x:=s[1].x-1;
for i:=1 to 10 do
begin
if (s[1].y=p[i].y) and (s[1].x=p[i].x) then begin h:=h+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=2 to h do
begin
if (s[1].y=s[i].y) and (s[1].x=s[i].x) then hal:=true;
end;
If s[1].x> 49 then hal:=true;
If s[1].y> 24 then hal:=true;
If s[1].y < 3 then hal:=true;
If s[1].x < 2 then hal:=true;
If (s[1].x>=30) and (s[1].x<=39) and (s[1].y>=10) and (s[1].y<=20) then hal:=true;
If t=0 then hal:=true;
textcolor(yellow);
for i:=1 to 10 do
begin
GotoXY(p[i].x,p[i].y);
write('*');
end;
textcolor(green);
GotoXY(s[1].x,s[1].y);
write('O');
If (irany='jobb') or (irany='bal') then delay(sebesseg)
else delay(sebesseg*2-20);

until hal;
textcolor(white);
textbackground(red);
GotoXY(23,12);
write('Game over');
textcolor(white);
textbackground(white);
delay(1500);
Rekh(h-kezdohossz);
End;

Procedure Classic();
Var s:Array [1..200] of snake;
    p: array [1..10] of snake;
    h,i,t:integer;
    c:char;
    hal:boolean;
    irany:string;
Begin
clrscr;
randomize;
h:=kezdohossz;
For i:=1 to 200 do s[i].y:=10+i;
For i:=1 to 200 do s[i].x:=20;
For i:=1 to 10 do p[i].x:=random(47)+2;
For i:=1 to 10 do p[i].y:=random(22)+3;
hal:=false;
irany:='';
t:=idohatar;
delay(100);
{pi:=0;}
for i:= 1 to 50 do begin GotoXY(i,2); write('-'); gotoXY(i,25); write('-'); end;
for i:=3 to 25 do begin GotoXY(1,i); write('|'); GotoXY(50,i); write('|'); end;
textcolor(green);
for i:=1 to h do
begin
GotoXY(s[i].x,s[i].y);
write('O');
end;
repeat
randomize;
{pi:=pi+1;
if pi MOD 24 =0 then begin
i:=random(10);
p[i].x:=random(47)+2;
p[i].y:=random(22)+3;
end;}
gotoXY(7,1);
textcolor(yellow);
textbackground(black);
write('SCORE: ',h-kezdohossz);
gotoXY(23,1); write('        ');
gotoXY(20,1);
write('x: ',s[1].x,' y: ',s[1].y);
t:=t-1;
gotoXY(41,1);
write('TIME: ',t);
if keypressed then c:= readkey;
case c of
#27: hal:=true;
#72: irany:='fel';
#80: irany:='le';
#77: irany:='jobb';
#75: irany:='bal';
else irany:='';
end;
If irany <> '' then begin
GotoXY(s[h].x,s[h].y);
write(' ');
for i:=h downto 2 do begin s[i].x:=s[i-1].x; s[i].y:=s[i-1].y;   end;
end;
If irany='fel' then s[1].y:=s[1].y-1;
If irany='le' then s[1].y:=s[1].y+1;
If irany='jobb' then s[1].x:=s[1].x+1;
If irany='bal' then s[1].x:=s[1].x-1;
for i:=1 to 10 do
begin
if (s[1].y=p[i].y) and (s[1].x=p[i].x) then begin h:=h+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=2 to h do
begin
if (s[1].y=s[i].y) and (s[1].x=s[i].x) then hal:=true;
end;
If s[1].x> 49 then hal:=true;
If s[1].y> 24 then hal:=true;
If s[1].y < 3 then hal:=true;
If s[1].x < 2 then hal:=true;
If t=0 then hal:=true;
textcolor(yellow);
for i:=1 to 10 do
begin
GotoXY(p[i].x,p[i].y);
write('*');
end;
textcolor(green);
GotoXY(s[1].x,s[1].y);
write('O');
If (irany='jobb') or (irany='bal') then delay(sebesseg)
else delay(sebesseg*2-20);

until hal;
textcolor(white);
textbackground(red);
GotoXY(23,12);
write('Game over');
textcolor(white);
textbackground(black);
delay(1500);
Rekh(h-kezdohossz);
End;




Procedure Gep();
Var s,s2:Array [1..200] of snake;
    p: array [1..10] of snake;
    h,h2,i,lk:integer;
    c:char;
    hal:boolean;
    irany,irany2:string;
Begin
clrscr;
randomize;
h:=kezdohossz;
h2:=kezdohossz;
For i:=1 to 200 do s[i].y:=10+i;
For i:=1 to 200 do s[i].x:=20;
For i:=1 to 200 do s2[i].y:=10+i;
For i:=1 to 200 do s2[i].x:=10;
For i:=1 to 10 do p[i].x:=random(47)+2;
For i:=1 to 10 do p[i].y:=random(22)+3;
hal:=false;
irany:='';
irany2:='';
delay(100);
 {pi:=0;}
for i:= 1 to 50 do begin GotoXY(i,2); write('-'); gotoXY(i,25); write('-'); end;
for i:=3 to 25 do begin GotoXY(1,i); write('|'); GotoXY(50,i); write('|'); end;
textcolor(green);
for i:=1 to h do
begin
GotoXY(s[i].x,s[i].y);
write('O');
end;
textcolor(red);
for i:=1 to h2 do
begin
GotoXY(s2[i].x,s2[i].y);
write('O');
end;
irany2:='fel';
repeat
{ pi:=pi+1;
if pi MOD 24 =0 then begin
i:=random(10);
p[i].x:=random(47)+2;
p[i].y:=random(22)+3;
end;}
randomize;
gotoXY(7,1);
textcolor(yellow);
textbackground(black);
write('SCORE: ',h+h2-kezdohossz*2);
if keypressed then c:= readkey;
case c of
#27: hal:=true;
#72: irany:='fel';
#80: irany:='le';
#77: irany:='jobb';
#75: irany:='bal';
end;
lk:=1;
For i:=1 to 10 do begin
if abs(s2[i].x-p[i].x)+abs(s2[i].y-p[i].y)<abs(s2[lk].x-p[lk].x)+abs(s2[lk].y-p[lk].y) then lk:=i;
end;
If p[lk].x>s2[1].x then irany2:='jobb'
  Else If p[lk].x<s2[1].x then irany2:='bal'
  Else begin
       If p[lk].y>s2[1].y then irany2:='le'
         Else If p[lk].y<s2[1].y then irany2:='fel';
       end;
If irany2='fel' then begin
i:=2;
while not((s2[i].x=s2[1].x) and (s2[i].y=s2[1].y-1)) and (i<=h2) do i:=i+1;
 if i<=h2 then begin
  if (s2[i+1].x<s2[1].x) or (s2[i-1].x>s2[1].x) then irany2:='bal'
  else irany2:='jobb';
 end;
end
Else If irany2='le' then begin
i:=2;
while not((s2[i].x=s2[1].x) and (s2[i].y=s2[1].y+1)) and (i<=h2) do i:=i+1;
 if i<=h2 then begin
  if (s2[i+1].x<s2[1].x) or (s2[i-1].x>s2[1].x) then irany2:='bal'
  else irany2:='jobb';
 end;
end
Else If irany2='jobb' then begin
i:=2;
while not((s2[i].y=s2[1].y) and (s2[i].x=s2[1].x+1)) and (i<=h2) do i:=i+1;
 if i<=h2 then begin
  if (s2[i+1].y<s2[1].y) or (s2[i-1].y>s2[1].y) then irany2:='fel'
  else irany2:='le';
 end;
end
Else If irany2='bal' then begin
i:=2;
while not((s2[i].y=s2[1].y) and (s2[i].x=s2[1].x-1)) and (i<=h2) do i:=i+1;
 if i<=h2 then begin
  if (s2[i+1].y<s2[1].y) or (s2[i-1].y>s2[1].y) then irany2:='fel'
  else irany2:='le';
 end;
end;
If (irany2='le') and (s2[1].y=24) then begin
i:=2;
while not((s2[i].y=s2[1].y) and (s2[i].x=s2[1].x-1)) and (i<=h2) do i:=i+1;
 If i<=h2 then irany2:='bal'
 else irany2:='jobb';
end
Else If (irany2='fel') and (s2[1].y=3) then begin
i:=2;
while not((s2[i].y=s2[1].y) and (s2[i].x=s2[1].x-1)) and (i<=h2) do i:=i+1;
 If i<=h2 then irany2:='jobb'
 else irany2:='bal';
end;
If irany2='fel' then begin
i:=2;
while not((s[i].x=s2[1].x) and (s[i].y=s2[1].y-1)) and (i<=h) do i:=i+1;
 if i<=h then begin
  i:=random(2);
  if i=1 then irany2:='bal'
  else irany2:='jobb';
 end;
end
Else If irany2='le' then begin
i:=2;
while not((s[i].x=s2[1].x) and (s[i].y=s2[1].y+1)) and (i<=h) do i:=i+1;
 if i<=h then begin
  i:=random(2);
  if i=1 then irany2:='bal'
  else irany2:='jobb';
 end;
end
Else If irany2='jobb' then begin
i:=2;
while not((s[i].y=s2[1].y) and (s[i].x=s2[1].x+1)) and (i<=h) do i:=i+1;
 if i<=h then begin
  i:=random(2);
  if i=1 then irany2:='fel'
  else irany2:='le';
 end;
end
Else If irany2='bal' then begin
i:=2;
while not((s[i].y=s2[1].y) and (s[i].x=s2[1].x-1)) and (i<=h) do i:=i+1;
 if i<=h then begin
  i:=random(2);
  if i=1 then irany2:='fel'
  else irany2:='le';
 end;
end;
If irany <> '' then begin
GotoXY(s[h].x,s[h].y);
write(' ');
for i:=h downto 2 do begin s[i].x:=s[i-1].x; s[i].y:=s[i-1].y;   end;
end;
If irany2 <> '' then begin
GotoXY(s2[h2].x,s2[h2].y);
write(' ');
for i:=h2 downto 2 do begin s2[i].x:=s2[i-1].x; s2[i].y:=s2[i-1].y;   end;
end;
If irany='fel' then s[1].y:=s[1].y-1;
If irany='le' then s[1].y:=s[1].y+1;
If irany='jobb' then s[1].x:=s[1].x+1;
If irany='bal' then s[1].x:=s[1].x-1;
If irany2='fel' then s2[1].y:=s2[1].y-1;
If irany2='le' then s2[1].y:=s2[1].y+1;
If irany2='jobb' then s2[1].x:=s2[1].x+1;
If irany2='bal' then s2[1].x:=s2[1].x-1;
for i:=1 to 10 do
begin
if (s[1].y=p[i].y) and (s[1].x=p[i].x) then begin h:=h+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=1 to 10 do
begin
if (s2[1].y=p[i].y) and (s2[1].x=p[i].x) then begin h2:=h2+1; p[i].x:=random(47)+2; p[i].y:=random(22)+3; end;
end;
for i:=2 to h do
begin
if (s[1].y=s[i].y) and (s[1].x=s[i].x) then hal:=true;
end;
for i:=2 to h2 do
begin
if (s2[1].y=s2[i].y) and (s2[1].x=s2[i].x) then begin h2:=h2-1; GotoXY(s2[h2+1].x,s2[h2+1].y); write(' '); end;
end;
for i:=2 to h do
begin
if (s2[1].y=s[i].y) and (s2[1].x=s[i].x) then hal:=true;
end;
for i:=2 to h2 do
begin
if (s[1].y=s2[i].y) and (s[1].x=s2[i].x) then hal:=true;
end;
If s[1].x> 49 then hal:=true;
If s[1].y> 24 then hal:=true;
If s[1].y < 3 then hal:=true;
If s[1].x < 2 then hal:=true;
If s2[1].x> 49 then hal:=true;
If s2[1].y> 24 then hal:=true;
If s2[1].y < 3 then hal:=true;
If s2[1].x < 2 then hal:=true;
textcolor(yellow);
for i:=1 to 10 do
begin
GotoXY(p[i].x,p[i].y);
write('*');
end;
textcolor(green);
GotoXY(s[1].x,s[1].y);
Write('O');
textcolor(red);
GotoXY(s2[1].x,s2[1].y);
Write('O');
delay(sebesseg+30);

until hal;
textcolor(white);
textbackground(red);
GotoXY(23,12);
write('Game over');
textcolor(white);
textbackground(white);
delay(1500);
Rekh(h+h2-kezdohossz*2);
End;




Procedure sugo();
Var f:text;
    a:string;
begin
clrscr;
assign(f,'sugo.txt');
reset(f);
while (NOT EOF(f)) do begin readln(f,a); writeln(AnsiToAscii(a)); end;
close(f);
readln;

end;


Procedure Palyak();
Var m,i:integer;
    c:char;
    mp:array [1..4] of string;
Begin
repeat
exit3:=false;
textbackground(black);
clrscr;
mp[1]:='1.pálya';
mp[2]:='2.pálya';
mp[3]:='3.pálya';
mp[4]:=' Vissza';
m:=0;
repeat
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('PÁLYÁK');
textcolor(white);
delay(100);
case c of
#80: inc(m);
#72: begin dec(m); m:=m+4; end;
end;
for i:=1 to 4 do
if i=((m MOD 4)+1) then begin textbackground(green); gotoXY(35,i*3); write(mp[i]); end
else begin textbackground(black); gotoXY(35,i*3); write(mp[i]); end;
c:=readkey;
until (c=#13) or (c=#27);
If c<>#27 then begin
case ((m MOD 4)+1) of
1: Classic();
2: P2();
3: P3;
4: exit3:=true;
end;
end
else exit3:=true;
until exit3;
end;

Procedure csere(var a,b:rek);
Var s:rek;
begin
s:=a;
a:=b;
b:=s;
end;

Procedure Rekh(const pont:integer);
Var i,j:integer;
Begin
textbackground(black);
clrscr;
assign(rk,'records.txt');
rewrite(rk);
rsz:=rsz+1;
reks[rsz].pontsz:=pont;
writeln('Rekord hozzáadása. Név:');
readln(reks[rsz].nev);
writeln(rk,rsz);
for i:=1 to rsz-1 do
 for j:=i+1 to rsz do
  If reks[i].pontsz<reks[j].pontsz then csere(reks[i],reks[j]);
for i:=1 to rsz do begin
writeln(rk,reks[i].nev);
writeln(rk,reks[i].pontsz);
end;
close(rk);
End;

Procedure Rekord();
Var i,j:integer;
Begin
assign(rk,'records.txt');
reset(rk);
clrscr;
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('Rekordok');
textcolor(white);
gotoxy(1,2);
readln(rk,rsz);
If rsz>0 then begin
for i:=1 to rsz do begin
readln(rk,reks[i].nev);
readln(rk,reks[i].pontsz);
end;
for i:=1 to rsz-1 do
 for j:=i+1 to rsz do
  If reks[i].pontsz<reks[j].pontsz then csere(reks[i],reks[j]);
For i:=1 to rsz do begin
writeln(i,'.: ',AnsiToAscii(reks[i].nev),'  ',reks[i].pontsz,' pont');
end;
end
else writeln('Még nincs tárolt rekord.');
close(rk);
readln;
End;


Procedure KRekord();
Var i,j:integer;
Begin
assign(rk,'records.txt');
reset(rk);
readln(rk,rsz);
for i:=1 to rsz do begin
readln(rk,reks[i].nev);
readln(rk,reks[i].pontsz);
end;
for i:=1 to rsz-1 do
 for j:=i+1 to rsz do
  If reks[i].pontsz<reks[j].pontsz then csere(reks[i],reks[j]);
close(rk);
End;

Procedure Seb();
Begin;
clrscr;
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('Sebesség beállítása');
textcolor(white);
cursoron;
gotoxy(1,2);
repeat
write('Haladási idő karakterenként: [>=40] ');
readln(sebesseg);
until sebesseg>=40;
cursoroff;
assign(bt,'settings.txt');
rewrite(bt);
writeln(bt,idohatar);
writeln(bt,sebesseg);
writeln(bt,kezdohossz);
close(bt);
end;

Procedure Idoh();
Begin;
clrscr;
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('Időhatár beállítása');
textcolor(white);
cursoron;
gotoxy(1,2);
repeat
write('Egyszemélyes játékban visszaszámlált idő: [>=500] ');
readln(idohatar);
until idohatar>=500;
cursoroff;
assign(bt,'settings.txt');
rewrite(bt);
writeln(bt,idohatar);
writeln(bt,sebesseg);
writeln(bt,kezdohossz);
close(bt);
end;

Procedure Kezdh();
Begin;
clrscr;
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('Kezdőhossz beállítása');
textcolor(white);
cursoron;
gotoxy(1,2);
repeat
write('Kígyó hossza a játék kezdetén: [<=10] ');
readln(kezdohossz);
until (kezdohossz<=10) and (kezdohossz>=2);
cursoroff;
assign(bt,'settings.txt');
rewrite(bt);
writeln(bt,idohatar);
writeln(bt,sebesseg);
writeln(bt,kezdohossz);
close(bt);
end;


Procedure Beal();
Var m,i:integer;
    c:char;
    mp:array [1..4] of string;
Begin
repeat
exit2:=false;
textbackground(black);
clrscr;
mp[1]:=' Sebesség ';
mp[2]:=' Időhatár ';
mp[3]:='Kezdőhossz';
mp[4]:='  Vissza  ';
m:=0;
repeat
gotoXY(37,1);
textbackground(black);
textcolor(red);
write('Beállítások');
textcolor(white);
delay(100);
case c of
#80: inc(m);
#72: begin dec(m); m:=m+4; end;
end;
for i:=1 to 4 do
if i=((m MOD 4)+1) then begin textbackground(green); gotoXY(35,i*3); write(mp[i]); end
else begin textbackground(black); gotoXY(35,i*3); write(mp[i]); end;
c:=readkey;
until (c=#13) or (c=#27);
If c<>#27 then begin
case ((m MOD 4)+1) of
1: Seb();
2: Idoh();
3: Kezdh();
4: exit2:=true;
end;
end
else exit2:=true;
until exit2;
end;


Procedure EgyszJatek();
Var m,i:integer;
    c:char;
    mp:array [1..4] of string;
Begin
repeat
exit2:=false;
textbackground(black);
clrscr;
mp[1]:='Klasszikus';
mp[2]:=' Gép ellen';
mp[3]:='  Pályák  ';
mp[4]:='  Vissza  ';
m:=0;
repeat
gotoXY(30,1);
textbackground(black);
textcolor(red);
write('EGYSZEMÉLYES JÁTÉK');
textcolor(white);
delay(100);
case c of
#80: inc(m);
#72: begin dec(m); m:=m+4; end;
end;
for i:=1 to 4 do
if i=((m MOD 4)+1) then begin textbackground(green); gotoXY(35,i*3); write(mp[i]); end
else begin textbackground(black); gotoXY(35,i*3); write(mp[i]); end;
c:=readkey;
until (c=#13) or (c=#27);
If c<>#27 then begin
case ((m MOD 4)+1) of
1: Classic();
2: Gep();
3: Palyak();
4: exit2:=true;
end;
end
else exit2:=true;
until exit2;
end;

Procedure menu();
Var m,i:integer;
    c:char;
    mp:array [1..6] of string;
Begin
repeat
textbackground(black);
clrscr;
cursoroff;
mp[1]:='Egy játékos';
mp[2]:='Két játékos';
mp[5]:='    Súgó   ';
mp[6]:='  Kilépés  ';
mp[3]:='  Rekordok ';
mp[4]:='Beállítások';
m:=0;
gotoXY(38,1);
textbackground(black);
textcolor(green);
write('SNAKE');
exit1:=false;
repeat
textcolor(white);
delay(100);
case c of
#80: inc(m);
#72: begin dec(m); m:=m+6; end;
end;
for i:=1 to 6 do
if i=((m MOD 6)+1) then begin textbackground(green); gotoXY(35,i*3); write(mp[i]); end
else begin textbackground(black); gotoXY(35,i*3); write(mp[i]); end;
c:=readkey;
If c=#27 then exit1:=true;
until (c=#13) or (c=#27);
If c<>#27 then begin
case ((m MOD 6)+1) of
1: EgyszJatek;
2: Ketsz();
5: sugo();
4: Beal();
3: Rekord();
6: exit1:=true;
end;
end;
If c=#27 then exit1:=true;
until exit1;
end;

BEGIN
KRekord();
assign(bt,'settings.txt');
reset(bt);
readln(bt,idohatar);
readln(bt,sebesseg);
readln(bt,kezdohossz);
close(bt);
menu();
END.
